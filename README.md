Edgii is an [OpenSource] (https://opensource.com/) ([MPL2] (http://mozilla.org/MPL/2.0/)) web application which runs on a Rasberry Pi and gives you an easy way to connect to it on the local network or access it over the Internet when you're away using the [Edgii.io] (http://edgii.io) website .  Supports a simple motion + webcam application and installation of [openHab home automation software] (http://www.openhab.org/) as well as easy remote access to the command line.

To get started there's some [stuff you may need] (https://gitlab.com/abelii/edgii/wikis/the-stuff-you'll-need)

Once you have your stuff you can [begin your installation] (https://gitlab.com/abelii/edgii/wikis/install-page)

If you have questions or need some help [you can ask here] (https://stackoverflow.com/questions/tagged/edgii)

If you find an issue please [let us know] (https://gitlab.com/abelii/edgii/issues)

... and here is [the authoritative version of this page] (https://gitlab.com/abelii/edgii/wikis/home)
